<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsuariosController extends Controller
{
     public function index(){


        return view('admin.usuarios.index');
     }
     public function cadastrar(){


        return view('admin.usuarios.cadastrar');

     }
     public function editar(){

        return view('admin.usuarios.editar');

     }
     public function visualizar(){

        return view('admin.usuarios.visualizar');

     }
     public function deletar(){
        
        echo "Função Deletar";

     }
}
